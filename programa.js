class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atrubutosProveedor;
    marcadores=[];
    circulos=[];
    poligonos= [];
    //Constructor (se ejecuta inmediantamente)

    constructor(){

        //Asignar a un objeto de la clase mapa uno de la biblioteca Leaflet

        this.posicionInicial=[4.653599,-74.154557];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atrubutosProveedor={
            maxZoom:20
        }

        //Llamar a las funciones del objeto
        //Atrubutos de la vista
        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atrubutosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }
    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);

    }
    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }
    colocarPoligono(posicion, configuracion){

        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
}

let miMapa=new Mapa();

//Marcador casa, parque y biblioteca tintal
miMapa.colocarMarcador([4.645584,-74.163421]);
miMapa.colocarCirculo([4.645584,-74.163421], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
});
miMapa.colocarPoligono(
    [
        [4.620282,-74.136985],
        [4.618165,-74.137189],
        [4.617812,-74.137092],
        [4.617641,-74.136717],
        [4.617855,-74.133336],
        [4.625212,-74.133519],
        [4.625501,-74.136428],
    ]
);
